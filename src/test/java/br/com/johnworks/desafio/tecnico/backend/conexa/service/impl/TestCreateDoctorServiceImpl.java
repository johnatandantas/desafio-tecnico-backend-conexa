package br.com.johnworks.desafio.tecnico.backend.conexa.service.impl;

import br.com.johnworks.desafio.tecnico.backend.conexa.exception.BusinessException;
import br.com.johnworks.desafio.tecnico.backend.conexa.model.Doctor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.time.LocalDate;
import java.util.UUID;

@SpringBootTest
@ActiveProfiles("test")
@TestPropertySource("/application-test.yml")
public class TestCreateDoctorServiceImpl {

    @Autowired
    private CreateDoctorServiceImpl createDoctorService;


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    void tearDown() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "schedules");
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "doctors");
    }

    @Test
    void shouldSavedDoctor() {
        Doctor doctor = getDoctor();

        final var doctorSaved = createDoctorService.execute(doctor, "123456");

        Assertions.assertNotNull(doctorSaved.getId());

    }

    @Test
    void shouldReturnErrorDifferentPassword() {
        Doctor doctor = Doctor.builder()
                .password("123456")
                .build();


        Exception exception = Assertions.assertThrows(BusinessException.class, () -> {
            createDoctorService.execute(doctor, "1234567");
        });

        String expectedMessage = "The password and confirmation password do not match.";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    void shouldReturnErrorExistsDoctor() {
        Doctor doctor01 = getDoctor();
        Doctor doctor02 = getDoctor();

        createDoctorService.execute(doctor01, "123456");


        Exception exception = Assertions.assertThrows(BusinessException.class, () -> {
            createDoctorService.execute(doctor02, "123456");
        });

        String expectedMessage = "Already a doctor with this document or registered email.";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));

    }

    private Doctor getDoctor() {
        return Doctor.builder()
                .doctorId(UUID.randomUUID().toString())
                .document("04276666007")
                .password("123456")
                .dateOfBirth(LocalDate.now())
                .email("teste@email.com.br")
                .phone("(99) 99999-9999")
                .specialty("cardiologista")
                .build();
    }

}
