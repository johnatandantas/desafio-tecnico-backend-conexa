package br.com.johnworks.desafio.tecnico.backend.conexa.service.impl;

import br.com.johnworks.desafio.tecnico.backend.conexa.model.Doctor;
import br.com.johnworks.desafio.tecnico.backend.conexa.model.Schedule;
import br.com.johnworks.desafio.tecnico.backend.conexa.repository.DoctorRepository;
import br.com.johnworks.desafio.tecnico.backend.conexa.security.JwtUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@SpringBootTest
@ActiveProfiles("test")
@TestPropertySource("/application-test.yml")
public class TestCreateScheduledServiceImpl {

    @Autowired
    private CreateScheduledServiceImpl createScheduledService;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private JwtUtil jwtUtil;

    @BeforeEach
    void tearDown() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "schedules");
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "doctors");
    }

    @Test
    void shouldSavedSchedule() {
        doctorRepository.save(getDoctor());
        var token = "Bearer " + jwtUtil.generateToken("teste@email.com.br");
        final var scheduled = createScheduledService.execute(getScheduled(), token);
        Assertions.assertNotNull(scheduled.getId());
    }

    private Doctor getDoctor() {
        return Doctor.builder()
                .doctorId(UUID.randomUUID().toString())
                .document("04276666007")
                .password("123456")
                .dateOfBirth(LocalDate.now())
                .email("teste@email.com.br")
                .phone("(99) 99999-9999")
                .specialty("cardiologista")
                .build();
    }

    private Schedule getScheduled() {
        return Schedule.builder()
                .scheduleId(UUID.randomUUID().toString())
                .name("Joao K")
                .document("04276666006")
                .scheduling(LocalDateTime.now())
                .build();
    }
}
