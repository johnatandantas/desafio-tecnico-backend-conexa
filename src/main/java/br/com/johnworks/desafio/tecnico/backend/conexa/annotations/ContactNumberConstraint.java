package br.com.johnworks.desafio.tecnico.backend.conexa.annotations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ContactNumberValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ContactNumberConstraint {

    String message() default "Invalid phone number (xx) xxxxx-xxxx";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
