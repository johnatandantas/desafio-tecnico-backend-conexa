package br.com.johnworks.desafio.tecnico.backend.conexa.api.controller;

import br.com.johnworks.desafio.tecnico.backend.conexa.api.request.CreateScheduledRequest;
import br.com.johnworks.desafio.tecnico.backend.conexa.service.CreateScheduledService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AllArgsConstructor
@Validated
@RestController
@RequestMapping("attendance")
public class ScheduledController {

    private CreateScheduledService createScheduledService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void scheduled(
            @RequestHeader("Authorization") String token,
            @RequestBody @Valid CreateScheduledRequest request) {
        createScheduledService.execute(request.toModel(), token);
    }

}
