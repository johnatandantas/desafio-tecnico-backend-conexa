package br.com.johnworks.desafio.tecnico.backend.conexa.api.request;

import br.com.johnworks.desafio.tecnico.backend.conexa.model.Schedule;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.Valid;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Getter
@Setter
public class CreateScheduledRequest {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @FutureOrPresent(message = "should is date present or future")
    @JsonProperty("dataHora")
    private LocalDateTime dateHour;

    @Valid
    @JsonProperty("paciente")
    private Patient patient;

    public static class Patient {

        @NotBlank
        @JsonProperty("nome")
        private String name;

        @CPF(message = "cpf invalid")
        @NotBlank
        @JsonProperty("cpf")
        private String document;
    }


    public Schedule toModel() {
        Schedule schedule = new Schedule();
        schedule.setScheduling(this.dateHour);
        schedule.setDocument(this.patient.document);
        schedule.setName(this.patient.name);

        return schedule;
    }

}
