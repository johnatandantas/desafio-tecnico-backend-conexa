package br.com.johnworks.desafio.tecnico.backend.conexa.exception;

public class AuthenticationException extends RuntimeException {

    public AuthenticationException(String message) {
        super(message);
    }
}
