package br.com.johnworks.desafio.tecnico.backend.conexa.api.controller;

import br.com.johnworks.desafio.tecnico.backend.conexa.api.request.CreateDoctorRequest;
import br.com.johnworks.desafio.tecnico.backend.conexa.model.RevokeToken;
import br.com.johnworks.desafio.tecnico.backend.conexa.repository.RevokeTokenRepository;
import br.com.johnworks.desafio.tecnico.backend.conexa.service.CreateDoctorService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AllArgsConstructor
@Validated
@RestController
@RequestMapping
public class AuthenticationController {

    private CreateDoctorService createDoctorService;

    private RevokeTokenRepository revokeTokenRepository;

    @PostMapping("signup")
    @ResponseStatus(HttpStatus.CREATED)
    public void signup(@RequestBody @Valid CreateDoctorRequest createDoctorInput) {
        createDoctorService.execute(createDoctorInput.toModel(), createDoctorInput.getConfirmPassword());
    }

    @DeleteMapping("logoff")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void logoff(@RequestHeader("Authorization") String token) {
        revokeTokenRepository.save(RevokeToken.builder().token(token.split(" ")[1]).build());
    }
}
