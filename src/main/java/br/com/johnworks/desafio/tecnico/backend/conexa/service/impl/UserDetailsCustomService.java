package br.com.johnworks.desafio.tecnico.backend.conexa.service.impl;

import br.com.johnworks.desafio.tecnico.backend.conexa.security.UserCustomDetails;
import br.com.johnworks.desafio.tecnico.backend.conexa.exception.AuthenticationException;
import br.com.johnworks.desafio.tecnico.backend.conexa.repository.DoctorRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class UserDetailsCustomService implements UserDetailsService {

    private DoctorRepository doctorRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final var doctor = doctorRepository.findByEmail(username).orElseThrow(() -> new AuthenticationException("User not found"));
        return new UserCustomDetails(doctor);
    }
}
