package br.com.johnworks.desafio.tecnico.backend.conexa.service.impl;

import br.com.johnworks.desafio.tecnico.backend.conexa.model.Schedule;
import br.com.johnworks.desafio.tecnico.backend.conexa.repository.DoctorRepository;
import br.com.johnworks.desafio.tecnico.backend.conexa.repository.ScheduledRepository;
import br.com.johnworks.desafio.tecnico.backend.conexa.security.JwtUtil;
import br.com.johnworks.desafio.tecnico.backend.conexa.service.CreateScheduledService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class CreateScheduledServiceImpl implements CreateScheduledService {

    private ScheduledRepository scheduledRepository;

    private DoctorRepository doctorRepository;

    private JwtUtil jwtUtil;

    @Override
    public Schedule execute(Schedule schedule, String token) {

        final var email = jwtUtil.getSubject(token.split(" ")[1]);

        final var doctor = doctorRepository.findByEmail(email);

        schedule.setDoctor(doctor.get());

        return scheduledRepository.save(schedule);
    }
}
