package br.com.johnworks.desafio.tecnico.backend.conexa.service;

import br.com.johnworks.desafio.tecnico.backend.conexa.model.Schedule;

public interface CreateScheduledService {

    Schedule execute(Schedule schedule, String token);
}
