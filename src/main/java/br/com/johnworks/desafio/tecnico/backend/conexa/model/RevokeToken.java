package br.com.johnworks.desafio.tecnico.backend.conexa.model;

import lombok.*;

import javax.persistence.*;


@Builder
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "revoke_token")
@NoArgsConstructor
@AllArgsConstructor
public class RevokeToken {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String token;

}
