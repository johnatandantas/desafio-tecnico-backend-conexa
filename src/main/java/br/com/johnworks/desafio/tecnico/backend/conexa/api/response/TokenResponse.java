package br.com.johnworks.desafio.tecnico.backend.conexa.api.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenResponse {
    private String token;

    public TokenResponse(String token) {
        this.token = "Bearer " + token;
    }
}
