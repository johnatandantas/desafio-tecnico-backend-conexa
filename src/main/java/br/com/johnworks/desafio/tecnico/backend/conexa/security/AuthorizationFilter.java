package br.com.johnworks.desafio.tecnico.backend.conexa.security;

import br.com.johnworks.desafio.tecnico.backend.conexa.exception.AuthenticationException;
import br.com.johnworks.desafio.tecnico.backend.conexa.repository.RevokeTokenRepository;
import br.com.johnworks.desafio.tecnico.backend.conexa.service.impl.UserDetailsCustomService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthorizationFilter extends BasicAuthenticationFilter {

    private final JwtUtil jwtUtil;
    private UserDetailsCustomService userDetailsCustomService;
    private RevokeTokenRepository revokeTokenRepository;

    public AuthorizationFilter(AuthenticationManager authenticationManager, JwtUtil jwtUtil, UserDetailsCustomService userDetailsCustomService, RevokeTokenRepository revokeTokenRepository) {
        super(authenticationManager);
        this.jwtUtil = jwtUtil;
        this.userDetailsCustomService = userDetailsCustomService;
        this.revokeTokenRepository = revokeTokenRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        var authorization = request.getHeader("Authorization");
        if (authorization != null && authorization.startsWith("Bearer ")) {
            var auth = getAuthentication(authorization.split(" ")[1]);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) {
        final var tokenOptional = revokeTokenRepository.findByToken(token);

        if (tokenOptional.isPresent()) {
            throw new AuthenticationException("Token Invalid");
        }

        if (!jwtUtil.isValidToken(token)) {
            throw new AuthenticationException("Token Invalid");
        }
        var subject = jwtUtil.getSubject(token);
        var doctor = userDetailsCustomService.loadUserByUsername(subject);
        return new UsernamePasswordAuthenticationToken(subject, null, null);
    }
}
