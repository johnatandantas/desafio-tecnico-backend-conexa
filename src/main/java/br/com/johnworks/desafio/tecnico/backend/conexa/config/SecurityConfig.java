package br.com.johnworks.desafio.tecnico.backend.conexa.config;

import br.com.johnworks.desafio.tecnico.backend.conexa.repository.DoctorRepository;
import br.com.johnworks.desafio.tecnico.backend.conexa.repository.RevokeTokenRepository;
import br.com.johnworks.desafio.tecnico.backend.conexa.security.AuthenticationFilter;
import br.com.johnworks.desafio.tecnico.backend.conexa.security.AuthorizationFilter;
import br.com.johnworks.desafio.tecnico.backend.conexa.security.JwtUtil;
import br.com.johnworks.desafio.tecnico.backend.conexa.service.impl.UserDetailsCustomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private UserDetailsCustomService userDetails;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private RevokeTokenRepository revokeTokenRepository;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetails).passwordEncoder(bCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable();
        http.authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/signup").permitAll()
                .and()
                .authorizeRequests()
                .anyRequest().authenticated();

        http.addFilter(new AuthenticationFilter(authenticationManagerBean(), doctorRepository, jwtUtil));
        http.addFilter(new AuthorizationFilter(authenticationManagerBean(), jwtUtil, userDetails, revokeTokenRepository));
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
