package br.com.johnworks.desafio.tecnico.backend.conexa.repository;

import br.com.johnworks.desafio.tecnico.backend.conexa.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduledRepository extends JpaRepository<Schedule, Long> {

}
