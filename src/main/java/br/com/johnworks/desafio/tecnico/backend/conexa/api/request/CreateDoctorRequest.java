package br.com.johnworks.desafio.tecnico.backend.conexa.api.request;

import br.com.johnworks.desafio.tecnico.backend.conexa.annotations.ContactNumberConstraint;
import br.com.johnworks.desafio.tecnico.backend.conexa.model.Doctor;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;
import org.modelmapper.ModelMapper;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@Getter
@Setter
public class CreateDoctorRequest {

    @Email(message = "Email should be valid")
    @NotBlank
    @JsonProperty("email")
    private String email;

    @NotBlank
    @JsonProperty("senha")
    private String password;

    @NotBlank
    @JsonProperty("confirmacaoSenha")
    private String confirmPassword;

    @NotBlank
    @JsonProperty("especialidade")
    private String specialty;

    @CPF
    @NotBlank
    @JsonProperty("cpf")
    private String document;

    @Past
    @JsonProperty("dataNascimento")
    private LocalDate dateOfBirth;

    @ContactNumberConstraint
    @NotBlank
    @JsonProperty("telefone")
    private String phone;

    public Doctor toModel() {
        return new ModelMapper().map(this, Doctor.class);
    }

}
