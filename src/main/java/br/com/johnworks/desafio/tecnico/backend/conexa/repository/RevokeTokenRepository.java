package br.com.johnworks.desafio.tecnico.backend.conexa.repository;

import br.com.johnworks.desafio.tecnico.backend.conexa.model.RevokeToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RevokeTokenRepository extends JpaRepository<RevokeToken, Long> {

    Optional<RevokeToken> findByToken(String Token);
}
