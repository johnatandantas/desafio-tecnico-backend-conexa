package br.com.johnworks.desafio.tecnico.backend.conexa.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {

    @JsonProperty("email")
    private String username;

    @JsonProperty("senha")
    private String password;
}
