package br.com.johnworks.desafio.tecnico.backend.conexa.repository.specification;

import br.com.johnworks.desafio.tecnico.backend.conexa.model.Doctor;
import org.springframework.data.jpa.domain.Specification;

public class DoctorSpecification {

    public static Specification<Doctor> byDocument(String document) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Doctor.DOCUMENT), document);
    }

    public static Specification<Doctor> byEmail(String email) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Doctor.EMAIL), email);
    }
}
