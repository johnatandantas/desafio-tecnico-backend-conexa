package br.com.johnworks.desafio.tecnico.backend.conexa.service;

import br.com.johnworks.desafio.tecnico.backend.conexa.model.Doctor;

public interface CreateDoctorService {

    Doctor execute(Doctor doctor, String confirmPassword);
}
