package br.com.johnworks.desafio.tecnico.backend.conexa.service.impl;

import br.com.johnworks.desafio.tecnico.backend.conexa.exception.BusinessException;
import br.com.johnworks.desafio.tecnico.backend.conexa.model.Doctor;
import br.com.johnworks.desafio.tecnico.backend.conexa.repository.DoctorRepository;
import br.com.johnworks.desafio.tecnico.backend.conexa.service.CreateDoctorService;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static br.com.johnworks.desafio.tecnico.backend.conexa.repository.specification.DoctorSpecification.byDocument;
import static br.com.johnworks.desafio.tecnico.backend.conexa.repository.specification.DoctorSpecification.byEmail;

@AllArgsConstructor
@Service
public class CreateDoctorServiceImpl implements CreateDoctorService {

    private DoctorRepository doctorRepository;

    @Override
    public Doctor execute(final Doctor doctor, final String confirmPassword) {

        if (!confirmPassword.equals(doctor.getPassword())) {
            throw new BusinessException("The password and confirmation password do not match.");
        }

        final var exitsDoctor = doctorRepository.findAll(byDocument(doctor.getDocument()).or(byEmail(doctor.getEmail())));

        if (!exitsDoctor.isEmpty()) {
            throw new BusinessException("Already a doctor with this document or registered email.");
        }

        final var passwordEncode = new BCryptPasswordEncoder().encode(doctor.getPassword());

        doctor.setPassword(passwordEncode);

        return doctorRepository.save(doctor);
    }
}
