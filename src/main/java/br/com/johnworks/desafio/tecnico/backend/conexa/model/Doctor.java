package br.com.johnworks.desafio.tecnico.backend.conexa.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "doctors")
public class Doctor {

    public static final String DOCUMENT = "document";
    public static final String EMAIL = "email";

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String doctorId;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String specialty;

    @Column(nullable = false)
    private String document;

    @Column(nullable = false)
    private LocalDate dateOfBirth;

    @Column(nullable = false)
    private String phone;

    @Version
    private Integer version;

    @PrePersist
    public void initializeIds() {
        if (Objects.isNull(doctorId)) {
            doctorId = "DCT-" + UUID.randomUUID().toString();
        }
    }
}
