package br.com.johnworks.desafio.tecnico.backend.conexa.security;

import br.com.johnworks.desafio.tecnico.backend.conexa.api.request.LoginRequest;
import br.com.johnworks.desafio.tecnico.backend.conexa.api.response.TokenResponse;
import br.com.johnworks.desafio.tecnico.backend.conexa.repository.DoctorRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final DoctorRepository doctorRepository;
    private JwtUtil jwtUtil;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {

        try {
            final var login = new ObjectMapper().readValue(request.getInputStream(), LoginRequest.class);

            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword());

            return authenticationManager.authenticate(authRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        final var username = ((UserCustomDetails) authResult.getPrincipal()).getUsername();

        final var token = jwtUtil.generateToken(username);

        TokenResponse tokenOutput = new TokenResponse(token);

        response.addHeader("Content-Type", "application/json");
        response.getWriter().write(new ObjectMapper().writeValueAsString(tokenOutput));
        response.getWriter().flush();

    }

}
