CREATE TABLE IF NOT EXISTS `schedules` (
`id`                    BIGINT          NOT NULL AUTO_INCREMENT ,
`schedule_id`           VARCHAR(100)    NOT NULL                ,
`name`                  VARCHAR(100)    NOT NULL                ,
`document`              VARCHAR(50)     NOT NULL                ,
`scheduling`            DATETIME        NOT NULL                ,
`doctor_id`              BIGINT          NOT NULL               ,
`version`               INT             NULL DEFAULT NULL       ,
PRIMARY KEY             (`id`)                                  ,
KEY         `FK_schedules_id` (`doctor_id`),
CONSTRAINT  `FK_schedules_id` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`),
INDEX `schedule_id`        (`schedule_id` ASC)
) ENGINE = InnoDB;
