CREATE TABLE IF NOT EXISTS `revoke_token` (
`id`                    BIGINT          NOT NULL AUTO_INCREMENT ,
`token`                 VARCHAR(500)    NOT NULL                ,
PRIMARY KEY             (`id`)                                  ,
INDEX `token`        (`token` ASC)
) ENGINE = InnoDB;
