CREATE TABLE IF NOT EXISTS `doctors` (
`id`                    BIGINT          NOT NULL AUTO_INCREMENT ,
`doctor_id`             VARCHAR(100)    NOT NULL                ,
`email`                 VARCHAR(100)    NOT NULL                ,
`password`              VARCHAR(100)    NOT NULL                ,
`specialty`             VARCHAR(255)    NOT NULL                ,
`document`              VARCHAR(50)     NOT NULL                ,
`phone`                 VARCHAR(100)    NULL                    ,
`date_of_birth`         DATETIME        NOT NULL                ,
`version`               INT             NULL DEFAULT NULL       ,
PRIMARY KEY             (`id`)                                  ,
INDEX `order_id`        (`doctor_id` ASC)
) ENGINE = InnoDB;
